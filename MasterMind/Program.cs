﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterMind
{
    class Program
    {
        static void Main(string[] args)
        {
            int randomnumber = GenerateRandomNumber();
            int[] randoms = randomnumber.ToString().ToCharArray().Select(x => (int)Char.GetNumericValue(x)).ToArray();
            List<string> saveChars = new List<string>();
            int maxChances = 10;
            bool success = false;
            for (int i = 0; i < 10; i++)
            {
                success = true;
                Console.WriteLine("Guess the 4 digit random number");
                string enteredNum = Console.ReadLine();
                maxChances--;
                if (enteredNum.Length == 4 && int.TryParse(enteredNum, out int result) && result > 0)
                {
                    int[] digits = enteredNum.ToCharArray().Select(x => (int)Char.GetNumericValue(x)).ToArray();
                    for (int val = 0; val < 4; val++)
                    {
                        if (randomnumber.ToString().Contains(digits[val].ToString()))
                        {
                            if (randoms[val] == digits[val])
                                saveChars.Add("+");
                            else
                            {
                                saveChars.Add("-");
                                success = false;
                            }
                        }
                        else
                            success = false;
                    }
                    if (success)
                    {
                        Console.WriteLine("You have entered correct number");
                        break;
                    }
                    else
                    {
                        saveChars.Sort();
                        saveChars.Reverse();
                        foreach (string item in saveChars)
                            Console.WriteLine(item);
                        saveChars.Clear();
                        Console.WriteLine($"You have entered wrong number. Please try again . You have {maxChances} chances left");
                    }
                }
                else
                {
                    success = false;
                    Console.WriteLine("Please enter 4 digit number");
                    Console.WriteLine($"Please try again . You have {maxChances} chances left");
                }
            }

            if (!success)
                Console.WriteLine("You've lost the game");
            Console.ReadLine();
        }
        
        public static int GenerateRandomNumber()
        {
            int _min = 1111;
            int _max = 6666;
            Random _rdm = new Random();
            bool isValid = true;
            int randomValue;
            do
            {
                randomValue = _rdm.Next(_min, _max);
                if (randomValue.ToString().Contains("0") || randomValue.ToString().Contains("7") || randomValue.ToString().Contains("8") || randomValue.ToString().Contains("9"))
                    isValid = false;
                else
                    isValid = true;

            } while (!isValid);

            return randomValue;
        }
    }
}
